package demo.test.exception;

public class StudentAlreadyRegisteredException extends RuntimeException {
    public StudentAlreadyRegisteredException(String msg) {
        super(msg);
    }
}
