package demo.test.student;

import java.util.List;

public interface StudentManagerService {
    void register(Student student);
    void remove(String id);
    Student get(String id);
    List<Student> getAllRegisteredStudents();
}
