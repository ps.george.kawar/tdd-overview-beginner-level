package demo.test.student;

import demo.test.exception.InvalidStudentDataException;
import demo.test.exception.StudentAlreadyRegisteredException;
import demo.test.exception.StudentNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentManagerServiceTest {

    StudentManagerService studentManager;

    @BeforeEach
    void init(){
        studentManager =  null; // Initialize student manager service here
    }

    @Test
    void givenNullStudent_whenRegister_thenThrowException(){
        InvalidStudentDataException exception = assertThrows(InvalidStudentDataException.class, () -> studentManager.register(null));
        assertEquals("Student must not be null", exception.getMessage());
    }

    @Test
    void givenNullId_whenRemove_thenThrowException(){
        InvalidStudentDataException exception = assertThrows(InvalidStudentDataException.class, () -> studentManager.remove(null));
        assertEquals("Student ID must not be null", exception.getMessage());
    }

    @Test
    void givenNullId_whenGet_thenThrowException(){
        InvalidStudentDataException exception = assertThrows(InvalidStudentDataException.class, () -> studentManager.get(null));
        assertEquals("Student ID must not be null", exception.getMessage());
    }

    @Test
    void givenID_whenRemove_thenThrowException(){
        StudentNotFoundException exception = assertThrows(StudentNotFoundException.class, () -> studentManager.remove("16700020"));
        assertEquals("Student with ID 16700020 is not found", exception.getMessage());

        exception = assertThrows(StudentNotFoundException.class, () -> studentManager.remove("123"));
        assertEquals("Student with ID 123 is not found", exception.getMessage());

        exception = assertThrows(StudentNotFoundException.class, () -> studentManager.remove("555"));
        assertEquals("Student with ID 555 is not found", exception.getMessage());
    }

    @Test
    void givenID_whenGet_thenThrowException(){
        StudentNotFoundException exception = assertThrows(StudentNotFoundException.class, () -> studentManager.get("16700020"));
        assertEquals("Student with ID 16700020 is not found", exception.getMessage());

        exception = assertThrows(StudentNotFoundException.class, () -> studentManager.get("123"));
        assertEquals("Student with ID 123 is not found", exception.getMessage());

        exception = assertThrows(StudentNotFoundException.class, () -> studentManager.get("55"));
        assertEquals("Student with ID 55 is not found", exception.getMessage());
    }

    @Test
    void givenStudent_whenRegisterAndGetAllRegisteredStudents_thenRegisterAndCheckNotExists(){
        Student student = new Student();
        student.setId("12345");
        student.setFirstName("George");
        student.setLastName("Kawar");
        student.setAddress("JO");
        student.setAge(20);
        studentManager.register(student);

        if(studentManager.getAllRegisteredStudents() == null){
            fail("Returned List is null");
        }

        Student expectedStudent = studentManager.getAllRegisteredStudents().get(0);
        assertEquals(expectedStudent, student);
        assertEquals(expectedStudent.hashCode(), student.hashCode());

        StudentAlreadyRegisteredException exception = assertThrows(StudentAlreadyRegisteredException.class, () -> studentManager.register(student));
        assertEquals("Student is already registered", exception.getMessage());

    }

    @Test
    void givenStudentAlreadyRegistered_whenRegister_thenThrowException(){
        Student student = new Student();
        student.setId("12345");
        student.setFirstName("George");
        student.setLastName("Kawar");
        student.setAddress("JO");
        student.setAge(20);
        studentManager.register(student);

        StudentAlreadyRegisteredException exception = assertThrows(StudentAlreadyRegisteredException.class, () -> studentManager.register(student));
        assertEquals("Student is already registered", exception.getMessage());
    }

    @Test
    void givenId_whenGet_thenReturnStudent(){
        Student student = new Student();
        student.setId("12345");
        student.setFirstName("George");
        student.setLastName("Kawar");
        student.setAddress("JO");
        student.setAge(20);
        studentManager.register(student);
        Student expectedStudent = studentManager.get("12345");

        assertEquals(expectedStudent.getId(), "12345");
        assertEquals(expectedStudent.getFirstName(), "George");
        assertEquals(expectedStudent.getLastName(), "Kawar");
        assertEquals(expectedStudent.getAddress(), "JO");
        assertEquals(expectedStudent.getAge(), 20);
    }

    @Test
    void givenId_whenRemove_thenRemoveStudent(){
        Student student = new Student();
        student.setId("12345");
        student.setFirstName("George");
        student.setLastName("Kawar");
        student.setAddress("JO");
        student.setAge(20);
        studentManager.register(student);
        studentManager.remove("12345");
        assertEquals(0, studentManager.getAllRegisteredStudents().size());
    }

}